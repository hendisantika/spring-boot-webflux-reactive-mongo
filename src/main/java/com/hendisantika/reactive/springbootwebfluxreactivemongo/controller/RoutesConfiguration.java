package com.hendisantika.reactive.springbootwebfluxreactivemongo.controller;

import com.hendisantika.reactive.springbootwebfluxreactivemongo.entity.Person;
import com.hendisantika.reactive.springbootwebfluxreactivemongo.repository.PersonRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;

import static org.springframework.web.reactive.function.server.RequestPredicates.method;
import static org.springframework.web.reactive.function.server.RequestPredicates.path;
import static org.springframework.web.reactive.function.server.RouterFunctions.nest;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;
import static org.springframework.web.reactive.function.server.ServerResponse.ok;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-webflux-reactive-mongo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-04
 * Time: 07:00
 * To change this template use File | Settings | File Templates.
 */
@Configuration
public class RoutesConfiguration {

    @Bean
    RouterFunction<?> routes(PersonRepository personRespository) {
        return nest(path("/person"),

                route(RequestPredicates.GET("/{id}"),
                        request -> ok().body(personRespository.findById(request.pathVariable("id")), Person.class))

                        .andRoute(method(HttpMethod.POST),
                                request -> {
                                    personRespository.insert(request.bodyToMono(Person.class)).subscribe();
                                    return ok().build();
                                })
        );
    }
}