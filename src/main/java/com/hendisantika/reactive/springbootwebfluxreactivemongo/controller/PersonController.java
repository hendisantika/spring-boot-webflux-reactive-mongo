package com.hendisantika.reactive.springbootwebfluxreactivemongo.controller;

import com.hendisantika.reactive.springbootwebfluxreactivemongo.entity.Person;
import com.hendisantika.reactive.springbootwebfluxreactivemongo.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-webflux-reactive-mongo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-04
 * Time: 07:03
 * To change this template use File | Settings | File Templates.
 */
@RestController
@RequestMapping("/person")
public class PersonController {

    @Autowired
    private PersonRepository personRespository;

    @GetMapping
    public Flux<Person> index() {
        return personRespository.findAll();
    }
}