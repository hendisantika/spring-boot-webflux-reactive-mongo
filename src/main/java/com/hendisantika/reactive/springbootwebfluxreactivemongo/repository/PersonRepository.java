package com.hendisantika.reactive.springbootwebfluxreactivemongo.repository;

import com.hendisantika.reactive.springbootwebfluxreactivemongo.entity.Person;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-webflux-reactive-mongo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-04
 * Time: 06:50
 * To change this template use File | Settings | File Templates.
 */
public interface PersonRepository extends ReactiveMongoRepository<Person, String> {
    Flux<Person> findByName(String name);
}
