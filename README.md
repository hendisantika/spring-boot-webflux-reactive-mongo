# Spring Boot Webflux Reactive Mongo
This is a sample application that shows how to build a web application using

1. Spring Boot 2

2. Spring Webflux

3. Spring Reactive Data MongoDb

4. Spring Security Reactive Webflux

Please see the following pages for more details
* Spring Web Reactive
http://docs.spring.io/spring-framework/docs/5.0.0.M1/spring-framework-reference/html/web-reactive.html

* Spring Data Reactive
https://spring.io/blog/2016/11/28/going-reactive-with-spring-data

* Spring Functional Web Framework
https://spring.io/blog/2016/09/22/new-in-spring-5-functional-web-framework

#### Running
In application.properties, configure appropriate values.

Run this using using the gradle wrapper included

`./gradlew clean bootRun`

And then go to http://localhost:8080 to test the API's.

#### cURL Commands
You can try the following API's once the server is running.

`GET /person`

`curl http://localhost:8080/person -v -u user:password`

`GET /person/{id}``

`curl http://localhost:8080/person/{id} -v -u admin:password`

`POST /person`

`curl -X POST -d '{"name":"Uzumaki Naruto","age":25}' -H "Content-Type: application/json" http://localhost:8080/person -v -u user:password`